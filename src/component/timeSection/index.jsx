import React, { useState } from "react";
import { Mask, Calendar, Button } from "antd-mobile";
import { RightOutline } from "antd-mobile-icons";
import "./index.css";
import dayjs from "dayjs";
import moment from "moment";

function WithContent(props) {
    const today = dayjs();
    const [val, setVal] = useState(() => [today.subtract(2, "day").toDate(), today.add(2, "day").toDate()]);
    // 确定
    const submit = () => {
        props.submit([moment(val[0]).format("YYYY-MM-DD"), moment(val[1]).format("YYYY-MM-DD")]);
    };
    // 重置
    const reset = () => {
        props.reset()
    }
    return (
        <>
            <Mask visible={props.visible} onMaskClick={() => props.close(false)}>
                <div className="calenderSection">
                    <div className="calender">
                        <Calendar
                            // defaultValue={defaultRange}
                            selectionMode="range"
                            value={val}
                            onChange={(val) => {
                                setVal(val);
                            }}
                        />
                        <div className="calenderBut">
                            <Button onClick={reset} style={{'marginRight': '10px'}}>
                                重置
                            </Button>
                            <Button color="primary" fill="solid" onClick={submit}>
                                确定
                            </Button>
                        </div>
                    </div>
                </div>
            </Mask>
        </>
    );
}

export default function TimeSection(props) {
    const [visible, setVisible] = useState(false);
    const [timeValue, setTimeValue] = useState("选择日期区间");
    const timeSectionClick = () => {
        setVisible(true);
    };
    const close = (val) => {
        setVisible(val);
    };
    const submit = (val) => {
        let time = "范围：" + moment(val[0]).format("YYYY-MM-DD") + " -- " + moment(val[1]).format("YYYY-MM-DD");
        setTimeValue(time);
        setVisible(false);
        props.timeSubmit(val)
    };
    const reset = () => {
        setTimeValue("选择日期区间")
        setVisible(false);
        props.timeReset()
    }
    return (
        <div>
            <div className="timeSection" onClick={timeSectionClick}>
                {timeValue} <RightOutline />
            </div>
            <WithContent visible={visible} close={close} submit={submit} reset={reset} />
        </div>
    );
}
