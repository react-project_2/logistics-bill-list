import React, { useState,useRef } from "react";
import { Dropdown, Divider } from "antd-mobile";
import "./index.css";

// 根据条件渲染激化组件
function Pitch({isPacked}) {
    if(isPacked) return <div className="pitch"></div>
}
function MyDivider({isPacked}) {
    if(isPacked) return <Divider />
}

export default function StateSelection(props) {
    const DropdownRef = useRef(null);
    const [title, setTitle] = useState('全部')
    const checkedItem = (item,index) => {
        console.log("需要观察这里的数据：",item,index)
        setTitle(item.label)
        DropdownRef.current.close();
    };
    return (
        <div className="stateSelection">
            <div className="column">包裹状态</div>
            <div className="column bardian">
                <Dropdown ref={DropdownRef}>
                    <Dropdown.Item key="sorter" title={title}>
                        <div style={{ padding: 12 }}>
                            {props.stateList.map((item, index) => {
                                return (
                                    <div className="dropdownItem" key={item.value}>
                                        <div className={item.label == title ? 'Item isActivated':'Item'} onClick={() => checkedItem(item,index)}>
                                            {item.label}
                                            <Pitch isPacked={item.label == title ? true : false} />
                                        </div>
                                        <MyDivider isPacked={index == props.stateList.length-1 ? false : true}/>
                                    </div>
                                );
                            })}
                        </div>
                    </Dropdown.Item>
                </Dropdown>
            </div>
        </div>
    );
}
