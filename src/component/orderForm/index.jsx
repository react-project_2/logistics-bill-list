import React, { useRef } from "react";
import { SearchBar, Divider } from "antd-mobile";
import "./index.css";
import { SearchBarRef } from "antd-mobile/es/components/search-bar";
import TimeSection from "../timeSection";
import StateSelection from "../stateSelection";
import Slot from "../../utils/slot";

export default function OrderForm(props) {
    const SearchRef = useRef(null);
    let timeValue = "";
    let searchValue = "";
    const onBlur = (val) => {
        if (val) {
            searchValue = val;
        } else {
            searchValue = SearchRef.current.nativeElement.value;
        }
        props.SearchBut(searchValue, timeValue)
    };
    const timeSubmit = (val) => {
        timeValue = val;
    };
    const timeReset = () => {
        timeValue = [];
    };
    return (
        <div className="orderForm">
            <div className="topColumn">
                <SearchBar
                    ref={SearchRef}
                    clearOnCancel={false}
                    style={{ "--padding-left": "8px" }}
                    placeholder="包裹收件人快递号收件人电话"
                    cancelText="搜索"
                    clearable
                    showCancelButton={() => true}
                    onCancel={() => {
                        onBlur();
                    }}
                    onSearch={(val) => {
                        onBlur(val);
                    }}
                />
                {Slot('searchBar',props)}
                <TimeSection timeSubmit={timeSubmit} timeReset={timeReset} />
                <Divider />
            </div>
            <StateSelection stateList={props.stateList} />
            <div className="content">{Slot('',props)}</div>
        </div>
    );
}
