import { sleep } from 'antd-mobile/es/utils/sleep'

const dataList = [
    {
        serve: '法邮海运',
        waybill: 'EN005163028FR',
        related: '',
        recipients: '元康',
        phone: '13466738843',
        date: '2022-01-12 03:57:30',
    },
    {
        serve: '法邮海运',
        waybill: 'EN005163014FR',
        related: '',
        recipients: '元康',
        phone: '13466738843',
        date: '2022-03-17 03:33:25',
    },
    {
        serve: '法邮海运',
        waybill: 'EN005163029FR',
        related: '',
        recipients: '元康',
        phone: '13466738843',
        date: '2022-01-17 03:02:57',
    },
    {
        serve: '法邮海运',
        waybill: 'EN005163028FR',
        related: '',
        recipients: '元康',
        phone: '13466738843',
        date: '2022-08-17 02:43:33',
    },
    {
        serve: '法邮海运',
        waybill: 'EN0051630006FR',
        related: '',
        recipients: '元康',
        phone: '13466738843',
        date: '2022-04-17 03:02:57',
    },
    {
        serve: '法邮海运',
        waybill: 'EN004163234FR',
        related: '',
        recipients: '元康',
        phone: '13466738843',
        date: '2022-05-17 02:43:33',
    }
]

let count = 0
const dataListDome = async () => {
    if (count >= 10) {
        return []
    }
    await sleep(1500)
    count++
    return dataList
}

const search = async (name, time) => {
    if (name == '' && time.length == 0) {
        return false
    }
    let newArr = []
    if (name != '' && time.length != 0) {
        let suitedList = dataList.filter(item => JSON.stringify(item).includes(name))
        newArr = suitedList.filter(item => {
            let data = {
                curDate: item.date,
                startDate: time[0],
                endDate: time[1],
            }
            return isDuringDate(data) && item
        })
        return newArr
    }
    if (name != '' && time.length == 0) {
        return newArr = dataList.filter(item => JSON.stringify(item).includes(name))
    }
    if (name == '' && time.length != 0) {
        newArr = dataList.filter(item => {
            let data = {
                curDate: item.date,
                startDate: time[0],
                endDate: time[1],
            }
            return isDuringDate(data) && item
        })
        return newArr
    }
}

const isDuringDate = (data) => {
    let curDate = new Date(data.curDate)
    let startDate = new Date(data.startDate)
    let endDate = new Date(data.endDate)
    if (curDate >= startDate && curDate <= endDate) {
        return true
    }
    return false
}

const stateList = [
    { label: '新订单', value: '1' },
    { label: '已揽收', value: '2' },
    { label: '运输中', value: '3' },
    { label: '派送中', value: '4' },
    { label: '全部', value: '5' },
]

export { dataListDome, stateList, search } 