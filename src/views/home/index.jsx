import React, { useState } from "react";
import OrderForm from "@/component/orderForm";
import { Button, InfiniteScroll, DotLoading } from "antd-mobile";
import { dataListDome, stateList, search } from "./dome";
import "./index.css";

function CardField({ dataList }) {
    const field = [
        { label: "服务", prop: "serve" },
        { label: "运单号(点击复制)", prop: "waybill" },
        { label: "关联运单", prop: "related" },
        { label: "收件人", prop: "recipients" },
        { label: "联系电话", prop: "phone" },
        { label: "下单日期", prop: "date" },
    ];
    return (
        <>
            {field.map((item) => {
                return (
                    <div className="field" key={item.prop}>
                        <span>{item.label}</span>
                        <span>{dataList[item.prop]}</span>
                    </div>
                );
            })}
        </>
    );
}

function Card(props) {
    return (
        <div className="cradDetails">
            <div className="fieldInfo">
                <CardField dataList={props.fieldData} />
            </div>

            <div className="butList">
                <Button size="mini" color="danger">
                    面单
                </Button>
                <Button size="mini" color="primary">
                    更多
                </Button>
            </div>
        </div>
    );
}

const InfiniteScrollContent = ({ hasMore }) => {
    return (
        <>
            {hasMore ? (
                <>
                    <span>Loading</span>
                    <DotLoading />
                </>
            ) : (
                <span>--- 我是有底线的 ---</span>
            )}
        </>
    );
};

export default function App() {
    const [data, setData] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    async function loadMore() {
        const append = await dataListDome();
        setData((val) => [...val, ...append]);
        setHasMore(append.length > 0);
    }
    async function SearchBut() {
        const append = await search(arguments[0], arguments[1]);
        if (append) {
            setData(append);
            setHasMore(false);
        } else {
            setData([]);
            loadMore();
            setHasMore(true);
        }
    }
    return (
        <div className="home">
            <OrderForm stateList={stateList} SearchBut={SearchBut}>
                {/* <div slot="searchBar">123</div> */}
                <>
                    {data.map((item, index) => {
                        return (
                            <div className="Card" key={index}>
                                <Card fieldData={item} />
                            </div>
                        );
                    })}
                    <InfiniteScroll loadMore={loadMore} hasMore={hasMore}>
                        <InfiniteScrollContent hasMore={hasMore} />
                    </InfiniteScroll>
                </>
            </OrderForm>
        </div>
    );
}
