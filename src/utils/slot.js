const Slot = (slotName, props) => {
    let children = props.children
    if (typeof children === 'object' && !Array.isArray(children)) children = [children]

    if (children)
        for (let el of children) {
            if (slotName == '') {
                if (!el.props.slot) return el
            } else {
                if (el.props.slot === slotName) return el
            }

        }
    return null
}

export default Slot