const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin');
const webpack = require('webpack'); //增加导入webpack

module.exports = {
    mode: 'development',
    devtool: "cheap-module-source-map",
    devServer: {
        //contentBase: path.join(__dirname, './src/'),
        //publicPath: '/',
        // historyApiFallback: true,
        hot: true,
        host: '127.0.0.1',
        port: 9888,
        //stats: {
        //    colors: true
        //}
    },
    // output: {
    //     path: path.resolve(__dirname, 'build'),
    //     publicPath: '/',
    // },
    entry: ['./src/index.jsx', './src/dev.js'], //在entry字段中添加触发文件配置
    resolve: {
        extensions: ['.wasm', '.mjs', '.js', '.json', '.jsx'],
        alias: {
            '@': path.resolve('src')
        }
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/, // jsx/js文件的正则
                exclude: /node_modules/, // 排除 node_modules 文件夹
                use: {
                    // loader 是 babel
                    loader: 'babel-loader',
                    options: {
                        // babel 转义的配置选项
                        babelrc: false,
                        presets: [
                            // 添加 preset-react
                            require.resolve('@babel/preset-react'),
                            [require.resolve('@babel/preset-env'), { modules: false }]
                        ],
                        // plugins: [
                        //     [
                        //         "import",
                        //         {libraryName:"antd", style: 'css'}
                        //     ]
                        // ],
                        cacheDirectory: true
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                use: 'url-loader'
            },
        ]
    },

    plugins: [
        // plugins中增加下面内容，实例化热加载插件
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebPackPlugin({
            template: 'public/index.html',
            filename: 'index.html',
            inject: true
        })
    ],
};